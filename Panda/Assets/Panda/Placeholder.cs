﻿using UnityEngine;
using UnityEngine.VR.WSA;
using UnityEngine.VR.WSA.Persistence;

public class Placeholder : MonoBehaviour
{
    public Vector3 Distance = new Vector3 { x = 0, y = 0, z = 2 };

    public GameObject KDR;

    private bool moving = false;
    private WorldAnchor anchor;
    private WorldAnchorStore store;

    // Use this for initialization
    void Start()
    {
        WorldAnchorStore.GetAsync(StoreLoaded);
        ShowPlaceholder(true);
    }

    void ShowPlaceholder(bool visible)
    {
        foreach (var r in gameObject.GetComponentsInChildren<Renderer>())
        {
            r.enabled = !visible;
        }
        foreach (var r in KDR.GetComponentsInChildren<Renderer>())
        {
            r.enabled = !visible;
        }
        GetComponent<Renderer>().enabled = visible;
    }

    private void StoreLoaded(WorldAnchorStore store)
    {
        this.store = store;
        Debug.Log($"Before: {gameObject.transform.position.x}, {gameObject.transform.position.y}, {gameObject.transform.position.z}");
        anchor = store.Load("Placeholder", gameObject);
        Debug.Log($"After: {gameObject.transform.position.x}, {gameObject.transform.position.y}, {gameObject.transform.position.z}");
        Debug.Log($"Anchro: {anchor.transform.position.x}, {anchor.transform.position.y}, {anchor.transform.position.z}");
    }

    // Update is called once per frame
    void Update()
    {
        if (moving)
        {
            var position = Camera.main.transform.position;
            var rotation = Camera.main.transform.forward;
            Debug.Log("Cam Position: " + position);
            Debug.Log("Cam invert: " + rotation);
            gameObject.transform.position = position + rotation * 2.5f;
            var viewVect = rotation;
            viewVect.y = 0;
            gameObject.transform.forward = viewVect;

            KDR.transform.position = position + rotation * 2.5f;
            KDR.transform.forward = viewVect;
        }
    }

    void OnPlace()
    {
        Debug.Log("OnPlace");
        moving = false;
        if (anchor != null)
        {
            DestroyImmediate(anchor);
        }
        anchor = gameObject.AddComponent<WorldAnchor>();
        if (store != null)
        {
            store.Save("Placeholder", anchor);
        }
        ShowPlaceholder(false);

    }

    void OnEnablePlacing()
    {
        Debug.Log("OnEnablePlacing");
        moving = true;
        ShowPlaceholder(true);
        if (anchor != null)
        {
            DestroyImmediate(anchor);
        }
    }
}
