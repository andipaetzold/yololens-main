﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Windows.Speech;

public class SpeechManager : MonoBehaviour
{
    [System.Serializable]
    public struct KeyPhraseBroadcast
    {
        [Tooltip("The phrase to be recognized.")]
        public string Phrase;
        [Tooltip("The UnityEvent to be invoked when the keyword is recognized.")]
        public UnityEvent Response;
    }

    [Tooltip("An array of string phrases and UnityEvents, to be set in the Inspector.")]
    public KeyPhraseBroadcast[] KeyPhraseBroadcasts;

    private KeywordRecognizer _keywordRecognizer;
    private Dictionary<string, UnityEvent> _unityEventResponse;

    // Initialize speech recognition
    void Start()
    {
        if (KeyPhraseBroadcasts == null || KeyPhraseBroadcasts.Length <= 0)
            return;

        // Create the phrase to broadcast mappings
        _unityEventResponse = KeyPhraseBroadcasts.ToDictionary(
            broadcast => broadcast.Phrase,
            broadcast => broadcast.Response);

        Debug.Log("Panda: Registered keys!");
        
        // Tell the KeywordRecognizer about our keywords.
        _keywordRecognizer = new KeywordRecognizer(_unityEventResponse.Keys.ToArray());

        // Register a callback for the KeywordRecognizer and start recognizing!
        _keywordRecognizer.OnPhraseRecognized += KeywordRecognizer_OnPhraseRecognized;
        _keywordRecognizer.Start();

        Debug.Log("Panda: Start instance!");
    }

    private void KeywordRecognizer_OnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        UnityEvent unityEvent;
        if (_unityEventResponse != null && _unityEventResponse.TryGetValue(args.text, out unityEvent))
        {
            unityEvent.Invoke();
            Debug.Log("Panda: Recognized text phrase and invoke unity event!");
        }
    }
    
}