﻿using HoloToolkit.Unity;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using SM = UnityEngine.SceneManagement;

public class SceneManager : Singleton<SceneManager>
{
    #region Unity Properties

    public GameObject LoadingText;

    #endregion

    private Text _loadingText;

    public void Start()
    {
        SetActiveOnChilds(false);
        if (LoadingText != null)
        {
            _loadingText = LoadingText.GetComponent<Text>();
        }
    }

    public void LoadScene(string sceneName)
    {
        StartCoroutine(LoadSceneAsync(sceneName));
    }

    private IEnumerator LoadSceneAsync(string sceneName)
    {
        SetActiveOnChilds(true);
        AsyncOperation asyncOp = SM.SceneManager.LoadSceneAsync(sceneName);
        while (!asyncOp.isDone)
        {
            var loadProgress = (int)(asyncOp.progress * 100);
            if (_loadingText != null)
            {
                _loadingText.text = $"{loadProgress} %";
            }
            yield return null;
        }
    }

    private void SetActiveOnChilds(bool active)
    {
        foreach (Transform child in transform)
        {
            var childRenderer = child.gameObject.GetComponent<Renderer>();
            if (childRenderer)
            {
                childRenderer.enabled = active;
            }
        }
    }
}
