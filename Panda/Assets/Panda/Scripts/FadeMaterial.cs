﻿using System;
using UnityEngine;

public class FadeMaterial : MonoBehaviour
{
    // public float Alpha = 1;
    private float _alpha;

    public bool Transparent;
    private bool _transparent;

    private Shader _transparentShader;
    private Shader _defaultShader;

    // Use this for initialization
    void Awake()
    {
        _transparentShader = Shader.Find("Transparent/Diffuse");
        _defaultShader = Shader.Find("Standard");
        // _alpha = Alpha + 1;
        _transparent = !Transparent;
    }

    // Update is called once per frame
    public void Update()
    {
        //disabled for test purposes
        if (_transparent != Transparent)
        {
            _transparent = Transparent;
            foreach (var childRenderer in GetComponentsInChildren<Renderer>())
            {
                childRenderer.material.shader = _transparent ? _transparentShader : _defaultShader;
            }
        }

        //if (Math.Abs(_alpha - Alpha) > 0.0001)
        //{
        //    _alpha = Alpha;
        //    foreach (var childRenderer in GetComponentsInChildren<Renderer>())
        //    {
        //        var color = childRenderer.material.color;
        //        childRenderer.material.color = new Color(color.r, color.g, color.b, Alpha);
        //    }
        //}
    }
}