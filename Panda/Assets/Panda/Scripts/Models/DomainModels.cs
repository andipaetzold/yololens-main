﻿using System;

namespace Assets.Panda.Scripts.Models
{
    #region JsonUtility Parser Models
    [Serializable]
    public class Action
    {
        public string text;
        public int sceneId;
        public object[] behaviour;
    }

    [Serializable]
    public class Step
    {
        public const int InvalidId = -1;
        public int id;
        public Action action;
        public int next; // TODO: nullable
        public int no; // TODO: nullable
        public int yes; // TODO: nullable
    }

    [Serializable]
    public class Scenario
    {
        public Step[] steps;
    }
    #endregion

    #region ScenarioManager Models
    public class StepTransition
    {
        public Step To { get; set; }
        public Step From { get; set; }
        public bool Reverse { get; set; }
    }
    #endregion
}
