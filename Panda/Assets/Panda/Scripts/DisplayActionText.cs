﻿using Assets.Panda.Scripts.Models;
using UnityEngine;
using UnityEngine.UI;

public class DisplayActionText : MonoBehaviour
{
    private const string DefaultStart = "Start the scenario!";

    public void OnStepTransition(StepTransition transition)
    {
        GetComponent<Text>().text = transition.To.action.text;
    }


    public void OnResetScenario()
    {
        GetComponent<Text>().text = DefaultStart;
    }
}
