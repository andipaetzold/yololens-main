﻿using System.Collections;
using System.Collections.Generic;
using Assets.Panda.Scripts.Models;
using HoloToolkit.Unity;
using UnityEngine;

public class AudioManager : Singleton<AudioManager>
{
    private AudioSource lastAudioSource = null;

    public void OnStepTransition(StepTransition transition)
    {
        var audioSource = gameObject.transform.FindChild($"Step{transition.To.id}").gameObject.GetComponent<AudioSource>();
        if (lastAudioSource != null)
        {
            lastAudioSource.Stop();
        }
        if (audioSource != null)
        {
            audioSource.Play();
            lastAudioSource = audioSource;
        }
    }

    public void OnResetScenario()
    {
        lastAudioSource = null;
    }
}
