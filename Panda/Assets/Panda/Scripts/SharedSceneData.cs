﻿using HoloToolkit.Unity;

public class SharedSceneData : Singleton<SharedSceneData>
{
    #region Unity Properties

    public string ScenarioScene;

    #endregion

    public void SetScenarioScene(string scene)
    {
        ScenarioScene = scene;
    }

    protected override void Awake()
    {
        base.Awake();
        DontDestroyOnLoad(gameObject);
    }
}
