﻿using HoloToolkit.Unity;
using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class PlaceObject : MonoBehaviour
{
    #region Unity Properties

    public string AnchorName;
    public float Distance = 2.5f;

    #endregion

    public bool Placing { get { return _placing; } }

    public bool AlreadyMoved { get; set; }

    private WorldAnchorManager _anchorManager;
    private GazeManager _gazeManager;
    private bool _placing;


    #region Default Unity Initializer

    public void Start()
    {
        AlreadyMoved = false;
        _anchorManager = WorldAnchorManager.Instance;
        if (_anchorManager == null)
        {
            Debug.LogError("Invalid WorldAnchorManager");
            Destroy(this);
            return;
        }

        _gazeManager = GazeManager.Instance;
        _anchorManager.AttachAnchor(gameObject, AnchorName);
    }

    #endregion

    public void Update()
    {
        if (_placing)
        {
            var gazePosition = Camera.main.transform.position;
            var gazeDirection = Camera.main.transform.forward;
            var gazeRotation = Camera.main.transform.rotation;

            if (_gazeManager != null && _gazeManager.Stabilizer != null)
            {
                var stabilizer = _gazeManager.Stabilizer;
                stabilizer.UpdateStability(gazePosition, gazeRotation);
                gazePosition = stabilizer.StablePosition;
                gazeDirection = stabilizer.StableRay.direction;
            }
            var viewDirection = gazeDirection;
            viewDirection.y = 0;

            gameObject.transform.position = gazePosition + gazeDirection * Distance;
            gameObject.transform.forward = viewDirection;
            AlreadyMoved = true;
        }
    }

    public void StartPlacing()
    {
        AlreadyMoved = false;
        _placing = true;
        if (_anchorManager != null && _anchorManager.AnchorStore != null)
        {
            _anchorManager.RemoveAnchor(gameObject);
        }
    }

    public void StopPlacing()
    {
        AlreadyMoved = false;
        _placing = false;
        if (_anchorManager != null && _anchorManager.AnchorStore != null)
        {
            _anchorManager.AttachAnchor(gameObject, AnchorName);
        }
    }
}
