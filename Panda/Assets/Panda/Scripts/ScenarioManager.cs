﻿using Assets.Panda.Scripts.Models;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
// ReSharper disable CheckNamespace


public class ScenarioManager : MonoBehaviour
{
    #region Unity Properties

    public TextAsset ScenarioFile;
    public GameObject[] ChangeStepListeners;

    #endregion

    public Scenario Scenario { get; private set; }

    public Step InitialStep { get; private set; }

    public Step CurrentStep { get; private set; }

    private Stack<Step> PreviousSteps { get; set; }

    private bool _resetScenario;

    #region Default Unity Initializer

    public void Start()
    {
        Scenario = JsonUtility.FromJson<Scenario>(ScenarioFile.text);
        InitialStep = Scenario.steps[0];
        PreviousSteps = new Stack<Step>(Scenario.steps.Length);
        ResetScenario();
    }

    #endregion

    public void ResetScenario()
    {
        _resetScenario = false;
        CurrentStep = null;
        PreviousSteps.Clear();
        NotifyListeners("OnResetScenario");
    }

    public void InitializeScenario()
    {
        SetCurrentStep(InitialStep);
    }

    public void NextStep()
    {
        if (CurrentStep == null ||
            CurrentStep.next == 0 ||
            CurrentStep.next == CurrentStep.id)
            return;
        if (_resetScenario)
        {
            _resetScenario = false;
            SetCurrentStep(InitialStep);
        }
        else
        {
            SetCurrentStep(GetStepById(CurrentStep.next));
        }
    }

    public void YesStep()
    {
        if (CurrentStep == null ||
            CurrentStep.yes == 0 ||
            CurrentStep.yes == CurrentStep.id)
            return;
        SetCurrentStep(GetStepById(CurrentStep.yes));
    }

    public void NoStep()
    {
        if (CurrentStep == null ||
            CurrentStep.no == 0 ||
            CurrentStep.no == CurrentStep.id)
            return;
        SetCurrentStep(GetStepById(CurrentStep.no));
    }

    public void PreviousStep()
    {
        if (PreviousSteps.Count == 0)
        {
            if (_resetScenario)
            {
                ResetScenario();
            }
            else
            {
                SetCurrentStep(InitialStep, false);
                _resetScenario = true;
            }
        }
        else
        {
            var previousStep = PreviousSteps.Pop();
            if (previousStep == null)
            {
                previousStep = InitialStep;
                _resetScenario = true;
            }
            SetCurrentStep(previousStep, false);
        }
    }

    private void SetCurrentStep(Step step, bool track = true)
    {
        var previousStep = CurrentStep;
        if (step == InitialStep)
        {
            PreviousSteps.Clear();
        }
        else
        {
            _resetScenario = false;
        }
        if (step == null) return;
        if (track && step != InitialStep)
        {
            PreviousSteps.Push(previousStep);
        }
        CurrentStep = step;
        if (previousStep == null && !track)
        {
            PreviousSteps.Clear();
            return;
        }
        NotifyListeners("OnStepTransition", new StepTransition
        {
            To = CurrentStep,
            From = previousStep,
            Reverse = !track
        });
    }

    private Step GetStepById(int id)
    {
        return Scenario.steps.FirstOrDefault(x => x.id == id);
    }

    private void NotifyListeners(string message)
    {
        foreach (var listeners in ChangeStepListeners)
        {
            listeners.SendMessage(message);
        }
    }

    private void NotifyListeners(string message, object data)
    {
        foreach (var listeners in ChangeStepListeners)
        {
            listeners.SendMessage(message, data);
        }
    }
}