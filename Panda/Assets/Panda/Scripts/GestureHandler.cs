﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.VR.WSA.Input;

public class GestureHandler : MonoBehaviour
{

    #region Unity Properties

    public UnityEvent Actions;
    public Color HighlightColor = new Color(1, 0, 0, 0.2f);

    #endregion

    private GazeManager _gazeManager;
    private GestureRecognizer _gestureRecognizer;

    private Color _defaultColor = Color.white;

    // Use this for initialization
    void Start()
    {
        var rendr = GetComponent<Renderer>();
        if (rendr != null && rendr.material != null)
        {
            _defaultColor = rendr.material.color;
        }

        _gazeManager = GazeManager.Instance;
        if (_gazeManager)
        {
            _gazeManager.FocusedObjectChanged += GazeManagerOnFocusedObjectChanged;
        }
        _gestureRecognizer = new GestureRecognizer();
        _gestureRecognizer.TappedEvent += GestureRecognizerOnTappedEvent;
        _gestureRecognizer.StartCapturingGestures();
    }

    private void GazeManagerOnFocusedObjectChanged(GameObject previousObject, GameObject newObject)
    {
        if (newObject == gameObject)
        {
            var rendr = GetComponent<Renderer>();
            if (rendr != null && rendr.material != null)
            {
                rendr.material.color = HighlightColor;
            }
        }
        else
        if (previousObject == gameObject)
        {
            var rendr = GetComponent<Renderer>();
            if (rendr != null && rendr.material != null)
            {
                rendr.material.color = _defaultColor;
            }
        }
    }

    private void GestureRecognizerOnTappedEvent(InteractionSourceKind source, int tapCount, Ray headRay)
    {
        if (_gazeManager == null || _gazeManager.HitObject != gameObject) return;
        if (Actions != null)
        {
            Actions.Invoke();
        }
    }

    void OnDestroy()
    {
        if (_gestureRecognizer != null)
        {
            _gestureRecognizer.StopCapturingGestures();
            _gestureRecognizer.TappedEvent -= GestureRecognizerOnTappedEvent;
        }

        if (_gazeManager != null)
        {
            _gazeManager.FocusedObjectChanged -= GazeManagerOnFocusedObjectChanged;
        }
    }
}
