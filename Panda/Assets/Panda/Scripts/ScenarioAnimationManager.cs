﻿using Assets.Panda.Scripts.Models;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public class ScenarioAnimationManager : MonoBehaviour
{
    private class AnimationInformation
    {
        public int Id { get; set; }
        public AnimationState State { get; set; }
        public bool ReverseLoop { get; set; }
    }

    public Animation Animation { get; private set; }

    private IDictionary<int, AnimationState> AnimationStates { get; set; }

    private Queue<KeyValuePair<int, bool>> _animations;

    private AnimationInformation _lastAnimation;

    #region Default Unity Initializer

    public void Awake()
    {
        Animation = GetComponent<Animation>();
        _animations = new Queue<KeyValuePair<int, bool>>();
    }

    public void Start()
    {
        if (Animation == null) return;
        AnimationStates = new Dictionary<int, AnimationState>(Animation.GetClipCount());
        foreach (AnimationState state in Animation)
        {
            var id = ExtractIdFromName(state.name);
            if (!AnimationStates.ContainsKey(id))
            {
                AnimationStates.Add(id, state);
            }
        }
        OnResetScenario();
    }
    #endregion

    public void Update()
    {
        if (!CanPlayAnimation() || _animations.Count <= 0) return;
        if (Animation != null && _lastAnimation != null && _lastAnimation.ReverseLoop)
        {
            // reset mode for reverse looping animations
            _lastAnimation.State.wrapMode = WrapMode.PingPong;
            _lastAnimation.ReverseLoop = false;
        }
        var nextAnimation = _animations.Dequeue();
        PlayAnimation(nextAnimation.Key, nextAnimation.Value);
    }

    public void OnStepTransition(StepTransition transition)
    {
        if (transition.Reverse)
        {
            EnqueueAnimation(transition.From.id, transition.Reverse);
            var animationState = GetAnimationStateById(transition.To.id);
            if (animationState == null) return;
            if (IsLoopingAnimation(animationState))
            {
                EnqueueAnimation(transition.To.id);
            }
        }
        else
        {
            EnqueueAnimation(transition.To.id);
        }
    }

    public void OnResetScenario()
    {
        if (AnimationStates == null) return;
        EnqueueAnimation(Step.InvalidId);
    }

    private void PlayAnimation(int id, bool reverse = false)
    {
        if (Animation == null) return;

        var animationState = GetAnimationStateById(id);
        if (animationState == null) return;

        var reverseLoop = false;
        if (reverse)
        {
            // prevent reverse looping animations
            if (IsLoopingAnimation(animationState))
            {
                reverseLoop = true;
            }
            animationState.wrapMode = WrapMode.Default;
            animationState.speed = -1;
            animationState.time = animationState.length;
        }
        else
        {
            animationState.speed = 1;
            animationState.time = 0;
        }

        _lastAnimation = new AnimationInformation
        {
            Id = id,
            ReverseLoop = reverseLoop,
            State = animationState
        };
        Animation.Play(animationState.name);
    }

    private int ExtractIdFromName(string name)
    {
        var match = Regex.Match(name, @"\d+");
        return match.Success ? int.Parse(match.Value) : Step.InvalidId;
    }

    private AnimationState GetAnimationStateById(int id)
    {
        var animationState = default(AnimationState);
        AnimationStates.TryGetValue(id, out animationState);
        return animationState;
    }

    private void EnqueueAnimation(int id, bool reverse = false)
    {
        if (id == Step.InvalidId)
        {
            Animation.Stop();
            _animations.Clear();
            _animations.Enqueue(new KeyValuePair<int, bool>(id, reverse));
        }
        else
        {
            _animations.Enqueue(new KeyValuePair<int, bool>(id, reverse));
        }
    }

    private bool CanPlayAnimation()
    {
        if (Animation == null) return false;
        if (_lastAnimation == null) return true;
        if (_lastAnimation.Id == Step.InvalidId ||
            _lastAnimation.State == null ||
            !Animation.isPlaying)
            return true;
        if (_lastAnimation.State == null) return true;
        return IsLoopingAnimation(_lastAnimation.State) &&
               HasAlreadyLooped(_lastAnimation.State);
    }

    private static bool IsLoopingAnimation(AnimationState animationState)
    {
        return animationState.wrapMode != WrapMode.Default &&
               animationState.wrapMode != WrapMode.Once;
    }

    private static bool HasAlreadyLooped(AnimationState animationState)
    {
        return !(animationState.normalizedTime < animationState.length) ||
               !(animationState.normalizedTime > -1);
    }
}
