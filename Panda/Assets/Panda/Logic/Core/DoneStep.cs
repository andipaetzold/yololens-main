﻿using System.Collections.Generic;
using Panda.Logic.Behavior;

namespace Panda.Logic.Core
{
    public class DoneStep : Step
    {
        public DoneStep(string text, int sceneId, HashSet<AbstractBehavior> behaviors)
            : base(text, sceneId, behaviors)
        {
        }

        public override HashSet<Command> AvailableCommands
        {
            get
            {
                return new HashSet<Command>();
            }
        }

        public override bool Equals(object obj)
        {
            if(!(obj is DoneStep))
            {
                return false;
            }

            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
