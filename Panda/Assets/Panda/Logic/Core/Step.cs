﻿using Panda.Logic.Behavior;
using System.Collections.Generic;

namespace Panda.Logic.Core
{
    public abstract class Step
    {
        protected Step(string text, int sceneId, HashSet<AbstractBehavior> behaviors)
        {
            Text = text;
            SceneId = sceneId;
            Behaviors = behaviors ?? new HashSet<AbstractBehavior>();
        }
        public int SceneId { get; private set; }
        public string Text { get; private set; }
        public HashSet<AbstractBehavior> Behaviors { get; private set; }
        public abstract HashSet<Command> AvailableCommands { get; }
        public override bool Equals(object obj)
        {
            if (!(obj is Step))
            {
                return false;
            }
            var stp = (Step)obj;

            return stp.Text.Equals(Text) && stp.SceneId.Equals(SceneId);
        }

        public override int GetHashCode()
        {
            return (SceneId.GetHashCode() + Text.GetHashCode()) % 2;
        }
    }
}
