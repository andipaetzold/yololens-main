﻿using Panda.Logic.Behavior;
using System;
using System.Collections.Generic;

namespace Panda.Logic.Core
{
    public class DecisionStep : Step
    {
        public DecisionStep(string text, int sceneId, HashSet<AbstractBehavior> behaviors, Step yesStep, Step noStep)
           : base(text, sceneId, behaviors)
        {
            Yes = yesStep;
            No = noStep;
        }
        public Step Yes { get; private set; }
        public Step No { get; private set; }
        public override HashSet<Command> AvailableCommands
        {
            get
            {
                var commands = new HashSet<Command>();
                commands.Add(Command.Yes);
                commands.Add(Command.No);
                return commands;
            }
        }

        public override bool Equals(object obj)
        {
            if(!(obj is DecisionStep))
            {
                return false;
            }
            return base.Equals(obj) && ((DecisionStep)obj).Yes.Equals(Yes) && ((DecisionStep)obj).No.Equals(No);
        }

        public override int GetHashCode()
        {
            return (Yes.GetHashCode() + No.GetHashCode()) % 2;
        }

        public void SetYes(Step yes)
        {
            if (Yes != null)
            {
                throw new Exception("Setting the yes step more than once is prohibited!");
            }
            Yes = yes;
        }

        public void SetNo(Step no)
        {
            if (No != null)
            {
                throw new Exception("Setting the no step more than once is prohibited!");
            }
            No = no;
        }
    }
}
