﻿using Panda.Logic.Behavior;
using System;
using System.Collections.Generic;

namespace Panda.Logic.Core
{
    public class ListStep : Step
    {
        public ListStep(string text, int sceneId, HashSet<AbstractBehavior> behaviors, Step nextStep)
            : base(text, sceneId, behaviors)
        {
            Next = nextStep;
        }
        public Step Next { get; private set; }

        public override HashSet<Command> AvailableCommands
        {
            get
            {
                var commands = new HashSet<Command>();
                commands.Add(Command.Next);
                return commands;
            }
        }

        public override bool Equals(object obj)
        {
            if(!(obj is ListStep))
            {
                return false;
            }

            return base.Equals(obj) && ((ListStep)obj).Next.Equals(Next);
        }

        public override int GetHashCode()
        {
            return Next.GetHashCode();
        }

        public void SetNext(Step next)
        {
            if(Next != null)
            {
                throw new Exception("Setting the next step more than once is prohibited!");
            }
            Next = next;
        }
    }
}
