﻿namespace Panda.Logic.Core
{
    public class Scenario
    {
        // TODO Datatype?
        public string MetaData { get; private set; }

        public Step FirstStep { get; private set; }

        public Scenario(Step firstStep)
        {
            FirstStep = firstStep;
        }

        public override bool Equals(object obj)
        {
            if(!(obj is Scenario))
            {
                return false;
            }
            var scen = (Scenario)obj;

            if((MetaData != null && scen.MetaData != null) && !MetaData.Equals(scen.MetaData))
            {
                return false;
            }

            if(!FirstStep.Equals(scen.FirstStep))
            {
                return false;
            }

            return true;
        }

        public override int GetHashCode()
        {
            return MetaData.GetHashCode() + FirstStep.GetHashCode();
        }
    }
}
