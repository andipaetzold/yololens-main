﻿namespace Panda.Logic.Behavior
{
    class DocumentBehavior : AbstractBehavior
    {
        public string DocumentURI { get; set; }
    }
}
