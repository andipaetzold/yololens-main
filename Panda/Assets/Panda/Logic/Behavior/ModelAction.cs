﻿using System.Collections.Generic;

namespace Panda.Logic.Behavior
{
    class ModelBehavior : AbstractBehavior
    {
        public int ModelId { get; set; }
        public string ScriptURI { get; set; }
        public List<string> Params { get; set; }
    }
}
