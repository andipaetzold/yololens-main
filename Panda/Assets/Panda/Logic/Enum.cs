﻿namespace Panda.Logic
{
    public enum Command
    {
        Help,
        Photo,
        Call,
        Close,
        Pause,
        DocumentNext,
        DocumentBack,
        DocumentClose,
        DocumentOpen,
        DocumentSearch,
        Next,
        Back, 
        Repeat,
        Yes,
        No
    };

    public static class CommandHelper
    {
        public static bool IsTransitionCommand(Command command)
        {
            switch(command)
            {
                case Command.Close:
                case Command.Next:
                case Command.Back:
                case Command.Yes:
                case Command.No:
                    return true;
            }
            return false;
        }
    }
}