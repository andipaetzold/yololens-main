﻿using System;
using System.Collections.Generic;
using Panda.Logic.Core;
using System.IO;
using Panda.Logic.Behavior;

namespace Panda.Logic.Service
{
    public class JSONScenarioLoaderService : IScenarioLoaderService
    {/**
        private Dictionary<int, Step> stepMap = new Dictionary<int, Step>();

        private HashSet<AbstractBehavior> ParseBehaviors(dynamic tkn)
        {
            return new HashSet<AbstractBehavior>();
        }
        public void ParseSteps(dynamic tkn)
        {
            Step tmp;
            foreach(var x in tkn)
            {
                if (x.next != null)
                {
                    tmp = new ListStep(x.action.text.ToString(), Convert.ToInt32(x.action.sceneId), ParseBehaviors(x.action.behavior), null);
                }
                // DecisionStep
                else if (x.yes != null && x.no != null)
                {
                    tmp = new DecisionStep(x.action.text.ToString(), Convert.ToInt32(x.action.sceneId), ParseBehaviors(x.action.behavior), null, null);
                }
                // DoneStep
                else
                {
                    tmp = new DoneStep(x.action.text.ToString(), Convert.ToInt32(x.action.sceneId), ParseBehaviors(x.action.behavior));
                }
                stepMap.Add(Convert.ToInt32(x.id), tmp);
            }
        }

        public void RepairReferences(dynamic tkn)
        {
            foreach(var x in tkn)
            {
                if(x.next != null)
                {
                    var stp =(ListStep) stepMap[Convert.ToInt32(x.id)];
                    stp.SetNext(stepMap[Convert.ToInt32(x.next)]);
                }
                else if(x.yes != null && x.no != null)
                {
                    var stp = (DecisionStep)stepMap[Convert.ToInt32(x.id)];
                    stp.SetYes(stepMap[Convert.ToInt32(x.yes)]);
                    stp.SetNo(stepMap[Convert.ToInt32(x.no)]);
                }
            }
        }*/

        public Scenario LoadScenario(string param)
        {/**
            if (!File.Exists(param))
            {
                throw new ArgumentException(string.Format("Scenario File \"{0}\" could not be found!", param), "param");
            }

            string jsonText = (new StreamReader(param)).ReadToEnd();
            
            dynamic jo = JValue.Parse(jsonText);

            int rootId = Convert.ToInt32(jo.steps.First.id);
            ParseSteps(jo.steps);
            RepairReferences(jo.steps);

            Step root = stepMap[rootId];
            return new Scenario(root);*/
            return null;
        }
    }
}
