﻿using Panda.Logic.Core;

namespace Panda.Logic.Service
{
    public interface IScenarioLoaderService
    {
        Scenario LoadScenario(string param);
    }
}
