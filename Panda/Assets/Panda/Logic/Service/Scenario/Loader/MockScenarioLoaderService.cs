﻿using System;
using Panda.Logic.Core;

namespace Panda.Logic.Service
{
    public class MockScenarioLoaderService : IScenarioLoaderService
    {
        public Scenario LoadScenario(string param)
        {
            switch (param)
            {
                case "demo":
                    var step7 = new DoneStep("Step 7 - done", 0, null);
                    var step6 = new DoneStep("Step 6 - done", 0, null);
                    var step5 = new DecisionStep("Step 5 - yes or no?", 0, null, step6, step7);
                    var step4 = new DoneStep("Step 4 - done", 0, null);
                    var step3 = new ListStep("Step 3 - list", 0, null, step4);
                    var step2 = new DecisionStep("Step 2 - yes or no?", 0, null, step3, step5);
                    var step1 = new ListStep("Step 1 - list", 0, null, step2);

                    return new Scenario(step1);
                default:
                    throw new ArgumentException(string.Format("Invalid parameter '{0}'.", param));
            }
        }
    }
}
