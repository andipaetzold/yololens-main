﻿namespace Panda.Logic.Service
{
    public static class ScenarioLoaderServiceFactory
    {
        public static IScenarioLoaderService CreateScenarioLoaderService()
        {
            return new MockScenarioLoaderService();
        }
    }
}
