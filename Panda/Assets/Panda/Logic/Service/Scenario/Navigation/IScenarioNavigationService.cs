﻿using System.Collections.Generic;
using Panda.Logic.Core;

namespace Panda.Logic.Service
{
    public interface IScenarioNavigationService
    {
        Scenario Scenario { get; set; }
        Step CurrentStep { get; set; }
        void CallCommand(Command command);
        HashSet<Command> AvailableCommands { get; }
    }
}
