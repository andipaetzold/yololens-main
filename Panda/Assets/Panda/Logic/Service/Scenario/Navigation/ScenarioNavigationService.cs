﻿using System;
using System.Collections.Generic;
using Panda.Logic.Core;

namespace Panda.Logic.Service
{
    public class ScenarioNavigationService : IScenarioNavigationService
    {
        private HashSet<Command> availableCommands = new HashSet<Command>();

        public Step CurrentStep { get; set; }

        private Scenario scenario;
        public Scenario Scenario
        {
            get
            {
                return scenario;
            }
            set
            {
                scenario = value;
                CurrentStep = scenario.FirstStep;
                ReloadAvailableCommands();
            }
        }

        public void CallCommand(Command command)
        {
            if (!AvailableCommands.Contains(command))
            {
                throw new InvalidOperationException(string.Format("Command '{0}' is illegal in the current context!", command));
            }

            // TODO: Implement all commands
            switch (command)
            {
                case Command.Next:
                    CurrentStep = (CurrentStep as ListStep).Next;
                    break;
                case Command.Yes:
                    CurrentStep = (CurrentStep as DecisionStep).Yes;
                    break;
                case Command.No:
                    CurrentStep = (CurrentStep as DecisionStep).No;
                    break;
                default:
                    throw new InvalidOperationException(string.Format("Invalid command '{0}'.", command));
            }


            if (CurrentStep is DoneStep)
            {
                // TODO Restart scenario
            }

            // Reload availableCommands after transition-command is called
            if (CommandHelper.IsTransitionCommand(command))
            {
                ReloadAvailableCommands();
            }
        }

        public void CallCommand(object next)
        {
            throw new NotImplementedException();
        }

        private void ReloadAvailableCommands()
        {
            availableCommands.Clear();
            foreach (var c in LoadAvailableCommands())
            {
                availableCommands.Add(c);
            }
        }

        private IEnumerable<Command> LoadAvailableCommands()
        {
            // Always possible commands
            yield return Command.Photo;
            yield return Command.Help;
            yield return Command.Call;
            yield return Command.Close;
            yield return Command.Pause;
            yield return Command.Repeat;

            // TODO check if document is opened
            if (false)
            {
                yield return Command.DocumentClose;
                yield return Command.DocumentSearch;

                // TODO Check if at the end of doc
                if (false)
                {
                    yield return Command.DocumentNext;
                }

                // TODO Check if at start of doc
                if (false)
                {
                    yield return Command.DocumentBack;
                }
            }
            else
            {
                yield return Command.DocumentOpen;
            }

            // Decision Step
            foreach (var c in CurrentStep.AvailableCommands)
            {
                yield return c;
            }

            if (Scenario.FirstStep != CurrentStep)
            {
                yield return Command.Back;
            }
        }

        public HashSet<Command> AvailableCommands
        {
            get
            {
                return availableCommands;
            }
        }

    }
}
