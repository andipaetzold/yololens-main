﻿using Panda.Logic.Core;
using System.Collections.Generic;

namespace Panda.Logic.Service
{
    interface IDocumentationService
    {
        void AddListener(IDocumentationListener listener);
        void RemoveListener(IDocumentationListener listener);
        void PushStep(Step step, List<string> resources);
    }
}
