﻿using Panda.Logic.Core;
using System.Collections.Generic;

namespace Panda.Logic
{
    interface IDocumentationListener
    {
        //TODO(Andi): Datatype (Resource???)
        void Update(Step step, List<string> resources);
    }
}
