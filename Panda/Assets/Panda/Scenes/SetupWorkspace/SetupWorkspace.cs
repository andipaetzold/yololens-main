﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.VR.WSA.Input;

public class SetupWorkspace : MonoBehaviour
{

    #region Unity Properties

    public GameObject SetupWorkspaceText;
    public GameObject Workspace;

    #endregion

    private SceneManager SceneManager {
        get { return SceneManager.Instance; }
    }

    private SharedSceneData SharedSceneData {
        get { return SharedSceneData.Instance; }
    }

    private GestureRecognizer _gestureRecognizer;

    public void Start()
    {
        _gestureRecognizer = new GestureRecognizer();
        _gestureRecognizer.TappedEvent += GestureRecognizerOnTappedEvent;
        _gestureRecognizer.StartCapturingGestures();
    }

    public void OnDestroy()
    {
        if (_gestureRecognizer != null)
        {
            _gestureRecognizer.StopCapturingGestures();
            _gestureRecognizer.TappedEvent -= GestureRecognizerOnTappedEvent;
        }
    }

    private void GestureRecognizerOnTappedEvent(InteractionSourceKind source, int tapCount, Ray headRay)
    {
        if (Workspace == null) return;
        var workspace = Workspace.GetComponent<PlaceObject>();
        if (workspace == null || !workspace.Placing || !workspace.AlreadyMoved) return;

        workspace.StopPlacing();
        var text = SetupWorkspaceText.GetComponent<Text>();
        if (text != null)
        {
            text.text = "Setup your workspace!";
        }
    }

    public void StartPlacing()
    {
        if (Workspace == null) return;
        var workspace = Workspace.GetComponent<PlaceObject>();
        if (workspace == null || workspace.Placing) return;
        workspace.StartPlacing();
        var text = SetupWorkspaceText.GetComponent<Text>();
        if (text != null)
        {
            text.text = "Use the Airtap gesture to place the Workspace!";
        }
    }

    public void LoadScenario()
    {
        if (SceneManager == null || SharedSceneData == null) return;
        SceneManager.LoadScene(SharedSceneData.ScenarioScene);
    }



}
