﻿using UnityEngine;

public class ScenarioMenu : MonoBehaviour
{
    public void LoadSlaveRoller()
    {
        SharedSceneData.Instance.SetScenarioScene("SlaveRoller");
        SceneManager.Instance.LoadScene("SetupWorkspace");
    }

    public void LoadMasterRoller()
    {
        SharedSceneData.Instance.SetScenarioScene("MasterRoller");
        SceneManager.Instance.LoadScene("SetupWorkspace");
    }

    public void LoadLightSensor()
    {
        SharedSceneData.Instance.SetScenarioScene("LightSensor");
        SceneManager.Instance.LoadScene("SetupWorkspace");
    }
}
