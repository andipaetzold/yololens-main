/label ~"Task"
/label ~"US*"
# Description

[Short description of the task.]

# Requirements 

[Requirements and dependencies this task has.]

# Concerned Users

[Insert user references (@...).]

# User story

[The user story this task corresponds to.]